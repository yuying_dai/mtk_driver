#ifdef MTK_LICENSE
/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002-2004, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

	Module Name:
	rtmp_init_inf.c

	Abstract:
	Miniport generic portion header file

	Revision History:
	Who         When          What
	--------    ----------    ----------------------------------------------
*/
#endif /* MTK_LICENSE */
#include	"rt_config.h"
#ifdef DOT11R_FT_SUPPORT
#include	"ft.h"
#endif /* DOT11R_FT_SUPPORT */

#ifdef CONFIG_STA_SUPPORT
#ifdef PROFILE_STORE
NDIS_STATUS WriteDatThread(RTMP_ADAPTER *pAd);
#endif /* PROFILE_STORE */
#endif /* CONFIG_STA_SUPPORT */

#ifdef MULTI_PROFILE
VOID multi_profile_exit(struct _RTMP_ADAPTER *ad);
#endif /*MULTI_PROFILE*/

#ifdef LINUX
#ifdef OS_ABL_FUNC_SUPPORT
/* Utilities provided from NET module */
RTMP_NET_ABL_OPS RtmpDrvNetOps, *pRtmpDrvNetOps = &RtmpDrvNetOps;
RTMP_PCI_CONFIG RtmpPciConfig, *pRtmpPciConfig = &RtmpPciConfig;
RTMP_USB_CONFIG RtmpUsbConfig, *pRtmpUsbConfig = &RtmpUsbConfig;

VOID RtmpDrvOpsInit(
	OUT VOID *pDrvOpsOrg,
	INOUT VOID *pDrvNetOpsOrg,
	IN RTMP_PCI_CONFIG *pPciConfig,
	IN RTMP_USB_CONFIG *pUsbConfig)
{
	RTMP_DRV_ABL_OPS *pDrvOps = (RTMP_DRV_ABL_OPS *)pDrvOpsOrg;
#ifdef RTMP_USB_SUPPORT
	RTMP_NET_ABL_OPS *pDrvNetOps = (RTMP_NET_ABL_OPS *)pDrvNetOpsOrg;
#endif /* RTMP_USB_SUPPORT */


	/* init PCI/USB configuration in different OS */
	if (pPciConfig != NULL)
		RtmpPciConfig = *pPciConfig;

	if (pUsbConfig != NULL)
		RtmpUsbConfig = *pUsbConfig;

	/* init operators provided from us (DRIVER module) */
	pDrvOps->RTMPAllocAdapterBlock = RTMPAllocAdapterBlock;
	pDrvOps->RTMPFreeAdapter = RTMPFreeAdapter;

	pDrvOps->RtmpRaDevCtrlExit = RtmpRaDevCtrlExit;
	pDrvOps->RtmpRaDevCtrlInit = RtmpRaDevCtrlInit;
#ifdef RTMP_MAC_PCI
	pDrvOps->RTMPHandleInterrupt = RTMPHandleInterrupt;
#endif /* RTMP_MAC_PCI */
	pDrvOps->RTMPSendPackets = RTMPSendPackets;
#ifdef P2P_SUPPORT
	pDrvOps->P2P_PacketSend = P2P_PacketSend;
#endif /* P2P_SUPPORT */

	pDrvOps->RTMP_COM_IoctlHandle = RTMP_COM_IoctlHandle;
#ifdef CONFIG_AP_SUPPORT
	pDrvOps->RTMP_AP_IoctlHandle = RTMP_AP_IoctlHandle;
#endif /* CONFIG_AP_SUPPORT */
#ifdef CONFIG_STA_SUPPORT
	pDrvOps->RTMP_STA_IoctlHandle = RTMP_STA_IoctlHandle;
#endif /* CONFIG_STA_SUPPORT */

	pDrvOps->RTMPDrvOpen = RTMPDrvOpen;
	pDrvOps->RTMPDrvClose = RTMPDrvClose;
	pDrvOps->RTMPInfClose = RTMPInfClose;
	pDrvOps->mt_wifi_init = mt_wifi_init;

	/* init operators provided from us and netif module */
#ifdef RTMP_USB_SUPPORT
	*pRtmpDrvNetOps = *pDrvNetOps;
	pRtmpDrvNetOps->RtmpDrvUsbBulkOutDataPacketComplete = RTUSBBulkOutDataPacketComplete;
	pRtmpDrvNetOps->RtmpDrvUsbBulkOutMLMEPacketComplete = RTUSBBulkOutMLMEPacketComplete;
	pRtmpDrvNetOps->RtmpDrvUsbBulkOutNullFrameComplete = RTUSBBulkOutNullFrameComplete;
/*	pRtmpDrvNetOps->RtmpDrvUsbBulkOutRTSFrameComplete = RTUSBBulkOutRTSFrameComplete;*/
	pRtmpDrvNetOps->RtmpDrvUsbBulkOutPsPollComplete = RTUSBBulkOutPsPollComplete;
	pRtmpDrvNetOps->RtmpDrvUsbBulkRxComplete = RTUSBBulkRxComplete;
#ifdef MT_MAC
    pRtmpDrvNetOps->RtmpDrvUsbBulkOutBCNPacketComplete = RTUSBBulkOutBCNPacketComplete;
#endif
	*pDrvNetOps = *pRtmpDrvNetOps;
#endif /* RTMP_USB_SUPPORT */
}

RTMP_BUILD_DRV_OPS_FUNCTION_BODY

#endif /* OS_ABL_FUNC_SUPPORT */
#endif /* LINUX */


INT rtmp_cfg_exit(RTMP_ADAPTER *pAd)
{
	UserCfgExit(pAd);

	return TRUE;
}


INT rtmp_cfg_init(RTMP_ADAPTER *pAd, RTMP_STRING *pHostName)
{
#ifndef MT7622 //wilsonl, skip for temp
	NDIS_STATUS status;
#endif

	UserCfgInit(pAd);

#ifdef FTM_SUPPORT
	FtmMgmtInit(pAd);
#endif /* FTM_SUPPORT */

#ifdef MESH_SUPPORT
	MeshCfgInit(pAd, pHostName);
#endif /* MESH_SUPPORT */

	CfgInitHook(pAd);

	/*
		WiFi system operation mode setting base on following partitions:
		1. Parameters from config file
		2. Hardware cap from EEPROM
		3. Chip capabilities in code
	*/
	if (pAd->RfIcType == 0) {
		/* RfIcType not assigned, should not happened! */
		pAd->RfIcType = RFIC_UNKNOWN;
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_OFF, ("%s(): Invalid RfIcType, reset it first\n",
					__FUNCTION__));
	}

#ifndef MT7622 //wilsonl, skip for temp
	status = RTMPReadParametersHook(pAd);
	if (status != NDIS_STATUS_SUCCESS)
	{
		MTWF_LOG(DBG_CAT_ALL, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("RTMPReadParametersHook failed, Status[=0x%08x]\n",status));
		return FALSE;
	}
#endif

	/*check all enabled function, decide the max unicast wtbl idx will use.*/
	/*After RTMPReadParameterHook to get MBSSNum & MSTANum*/
    HcSetMaxStaNum(pAd);

#ifdef DOT11_N_SUPPORT
   	/*Init Ba Capability parameters.*/
	pAd->CommonCfg.DesiredHtPhy.MpduDensity = (UCHAR)pAd->CommonCfg.BACapability.field.MpduDensity;
	pAd->CommonCfg.DesiredHtPhy.AmsduEnable = (USHORT)pAd->CommonCfg.BACapability.field.AmsduEnable;
	pAd->CommonCfg.DesiredHtPhy.AmsduSize = (USHORT)pAd->CommonCfg.BACapability.field.AmsduSize;
	pAd->CommonCfg.DesiredHtPhy.MimoPs = (USHORT)pAd->CommonCfg.BACapability.field.MMPSmode;
	/* Updata to HT IE*/
	pAd->CommonCfg.HtCapability.HtCapInfo.MimoPs = (USHORT)pAd->CommonCfg.BACapability.field.MMPSmode;
	pAd->CommonCfg.HtCapability.HtCapInfo.AMsduSize = (USHORT)pAd->CommonCfg.BACapability.field.AmsduSize;
	pAd->CommonCfg.HtCapability.HtCapParm.MpduDensity = (UCHAR)pAd->CommonCfg.BACapability.field.MpduDensity;
#endif /* DOT11_N_SUPPORT */

	return TRUE;
}


INT rtmp_mgmt_init(RTMP_ADAPTER *pAd)
{

	return TRUE;
}


static INT rtmp_sys_exit(RTMP_ADAPTER *pAd)
{
#ifdef SMART_ANTENNA
	RtmpSAExit(pAd);
#endif /* SMART_ANTENNA */

	MeasureReqTabExit(pAd);
	TpcReqTabExit(pAd);

#ifdef DOT11_N_SUPPORT
	if(pAd->mpdu_blk_pool.mem) {
		os_free_mem(pAd->mpdu_blk_pool.mem); /* free BA pool*/
		pAd->mpdu_blk_pool.mem = NULL;
	}
#endif /* DOT11_N_SUPPORT */

	rtmp_cfg_exit(pAd);
	HwCtrlExit(pAd);
	RTMP_CLEAR_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_REGISTER_TO_OS);
	RtmpMgmtTaskExit(pAd);
#ifdef RTMP_TIMER_TASK_SUPPORT
	NdisFreeSpinLock(&pAd->TimerQLock);
#endif
	return TRUE;
}


static INT rtmp_sys_init(RTMP_ADAPTER *pAd,RTMP_STRING *pHostName)
{
	NDIS_STATUS status;

	WifiSysInfoReset(&pAd->WifiSysInfo);
	WifiSysInfoDump(pAd);

	status = RtmpMgmtTaskInit(pAd);
	if (status != NDIS_STATUS_SUCCESS)
		goto err0;

	status = HwCtrlInit(pAd);
	if(status !=NDIS_STATUS_SUCCESS)
		goto err1;

	/* Initialize pAd->StaCfg[], pAd->ApCfg, pAd->CommonCfg to manufacture default*/
	if (rtmp_cfg_init(pAd, pHostName) != TRUE)
		goto err2;


#ifdef DOT11_N_SUPPORT
	/* Allocate BA Reordering memory*/
	if (ba_reordering_resource_init(pAd, MAX_REORDERING_MPDU_NUM) != TRUE)
		goto err2;
#endif /* DOT11_N_SUPPORT */

#ifdef BLOCK_NET_IF
	initblockQueueTab(pAd);
#endif /* BLOCK_NET_IF */

	status = MeasureReqTabInit(pAd);
	if (status != NDIS_STATUS_SUCCESS)
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("MeasureReqTabInit failed, Status[=0x%08x]\n", status));
		goto err2;
	}
	status = TpcReqTabInit(pAd);
	if (status != NDIS_STATUS_SUCCESS)
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("TpcReqTabInit failed, Status[=0x%08x]\n", status));
		goto err2;
	}

#ifdef SMART_ANTENNA
	RtmpSAInit(pAd);
#endif /* SMART_ANTENNA */

#ifdef MT_MAC
	/* TxS Setting */
	InitTxSTypeTable(pAd);
	if (pAd->chipCap.hif_type == HIF_MT)
	{
		InitTxSCommonCallBack(pAd);
	}
#endif

	/* hwnat optimize */
#if (!defined(CONFIG_RA_NAT_NONE)) || ( defined(BB_SOC) && defined(BB_RA_HWNAT_WIFI) )
			pAd->LanNatSpeedUpEn = 1;
#else
			pAd->LanNatSpeedUpEn = 0;
#endif
	pAd->CurWdevIdx = 0;
	pAd->isInitBrLan = 0;
	pAd->BrLanIpAddr = 0xffffffff;
	pAd->BrLanMask = 0xffffffff;
	return TRUE;


err2:
	rtmp_cfg_exit(pAd);
err1:
	HwCtrlExit(pAd);
err0:
	RTMP_CLEAR_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_REGISTER_TO_OS);
	RtmpMgmtTaskExit(pAd);
	return FALSE;
}

#ifdef NF_SUPPORT
VOID enable_nf_support(VOID *pAdSrc)
{
	RTMP_ADAPTER *pAd = (RTMP_ADAPTER *)pAdSrc;
	int Value ,i ;

	for(i=0;i < DBDC_BAND_NUM;i++)
	{
		/*band0*/
		pAd->Avg_NF[i] = 0;
		pAd->Avg_NFx16[i] = 0;

		if(i == DBDC_BAND0)
		{
			/*Turn on band 0 IPI*/
			HW_IO_READ32(pAd, PHY_RXTD_12, &Value);
			Value |= (1 << B0IrpiSwCtrlResetOffset);
			Value |= (1 << B0IrpiSwCtrlOnlyOffset);
			HW_IO_WRITE32(pAd, PHY_RXTD_12, Value);
			HW_IO_WRITE32(pAd, PHY_RXTD_12, Value);

			/* Enable badn0 IPI control */
			HW_IO_READ32(pAd, PHY_BAND0_PHYMUX_5, &Value);
			Value |= (B0IpiEnableCtrlValue << B0IpiEnableCtrlOffset);
			HW_IO_WRITE32(pAd, PHY_BAND0_PHYMUX_5, Value);
		}
		else{
		
			HW_IO_READ32(pAd, PHY_RXTD2_10 , &Value);
			Value |= (1 << B1IrpiSwCtrlResetOffset);
			Value |= (1 << B1IrpiSwCtrlOnlyOffset);
			HW_IO_WRITE32(pAd, PHY_RXTD2_10, Value);
			HW_IO_WRITE32(pAd, PHY_RXTD2_10, Value);
	
			HW_IO_READ32(pAd, PHY_BAND1_PHYMUX_5, &Value);
			Value |= (B1IpiEnableCtrlValue << B1IpiEnableCtrlOffset);
			HW_IO_WRITE32(pAd, PHY_BAND1_PHYMUX_5, Value);
		}
	}
}
#endif

/*rename from rt28xx_init*/
int mt_wifi_init(VOID *pAdSrc, RTMP_STRING *pDefaultMac, RTMP_STRING *pHostName)
{
#ifdef SMART_CARRIER_SENSE_SUPPORT
        UINT32  CrValue;
#endif /* SMART_CARRIER_SENSE_SUPPORT */
	RTMP_ADAPTER *pAd = (RTMP_ADAPTER *)pAdSrc;
	NDIS_STATUS Status;
//    UCHAR EDCCACtrl;
    UCHAR BandIdx = 0;

	if (!pAd)
		return FALSE;

#ifdef CONFIG_FWOWN_SUPPORT
	DriverOwn(pAd);
#endif

#if defined(RTMP_MAC) || defined(RLT_MAC)
	rtmp_chip_info_show(pAd);
#else
	mt_chip_info_show(pAd);
#endif

	// TODO: shiang-usw, need to check this for RTMP_MAC
	/* Disable interrupts here which is as soon as possible*/
	/* This statement should never be true. We might consider to remove it later*/
	if (RTMP_TEST_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_ACTIVE))
	{
		RTMP_ASIC_INTERRUPT_DISABLE(pAd);
	}

	/* reset Adapter flags */
	RTMP_CLEAR_FLAGS(pAd);

	/*for software system initialize*/
	if (rtmp_sys_init(pAd,pHostName) != TRUE)
		goto err2;

#ifdef RELEASE_EXCLUDE
	/* defined(RT5370) || defined(RT5372) || defined(RT5390) || defined(RT5392)

	     driver need backward compatible, so we need change these values dynamicly.
	*/
	/*
	2010/08/12:
		In windows's team, they found that the 5390 could not use the high memory.
		Becuase the AP will switch to high-memory to update the beacon every 100 ms in the power saving mode and
		then switch to low memory to update other information likes MCS. But our hardware does lock the high-low
		memory switch bit correctly, they encounter the mass contnet of memory in power-saving testing in WHQL.
		For fixing this bug, the hardware WAPI and Beacom will coexist in low-memory. Now we do not the same
		modifications, because we expect that we will not meet the same situation. We will use the original codes to
		pass QA test, if it could pass, we will decide to change the beacon buffer as Windows's team.
	*/
#endif // RELEASE_EXCLUDE //

#if 0
    /* TODO: out-of-date MAX_AVAILABLE_CLIENT_WCID need to be refine.*/
	if (MAX_LEN_OF_MAC_TABLE > MAX_AVAILABLE_CLIENT_WCID(pAd))
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("MAX_LEN_OF_MAC_TABLE can not be larger than MAX_AVAILABLE_CLIENT_WCID!!!!\n"));
		goto err2;
	}
#endif

	if((Status = WfInit(pAd))!=NDIS_STATUS_SUCCESS)
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("WfInit faild!!, ret=%d\n",Status));
		goto err2;
	}


	/* initialize MLME*/
	Status = MlmeInit(pAd);
	if (Status != NDIS_STATUS_SUCCESS)
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("MlmeInit failed, Status[=0x%08x]\n", Status));
		goto err3;
	}

	RTMP_SET_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_REGISTER_TO_OS);

#ifdef CONFIG_FPGA_MODE
#ifdef CAPTURE_MODE
	cap_mode_init(pAd);
#endif /* CAPTURE_MODE */
#endif /* CONFIG_FPGA_MODE */

#ifdef NF_SUPPORT
		/* Enable Noise Histrogram */
		MAC_IO_WRITE32(pAd, 0x12234, 0x07000000);
		MAC_IO_WRITE32(pAd, PHY_BAND0_PHYMUX_5, 0x50DC10); //Enabling IPI for Band 0
		if(pAd->CommonCfg.dbdc_mode){
			MAC_IO_WRITE32(pAd,0x12a2c,0x00000020);
			MAC_IO_WRITE32(pAd, PHY_BAND1_PHYMUX_5, 0x50DC10); //Enabling IPI for Band 1
		}
		NdisAllocateSpinLock(pAd, &pAd->ScanCtrl.NF_Lock);
#endif

	NICInitializeAsic(pAd);

#ifdef LED_CONTROL_SUPPORT
	/* Send LED Setting to MCU */
	RTMPInitLEDMode(pAd);
#endif /* LED_CONTROL_SUPPORT */

	tx_pwr_comp_init(pAd);

#ifdef WIN_NDIS
	/* Patch cardbus controller if EEPROM said so. */
	if (pAd->bTest1 == FALSE)
		RTMPPatchCardBus(pAd);
#endif /* WIN_NDIS */

#ifdef IKANOS_VX_1X0
	VR_IKANOS_FP_Init(pAd->ApCfg.BssidNum, pAd->PermanentAddress);
#endif /* IKANOS_VX_1X0 */

#ifdef CONFIG_ATE
	rtmp_ate_init(pAd);
#endif /*CONFIG_ATE*/

	/* Microsoft HCT require driver send a disconnect event after driver initialization.*/
	//STA_STATUS_CLEAR_FLAG(pStaCfg, fSTA_STATUS_MEDIA_STATE_CONNECTED);
	OPSTATUS_CLEAR_FLAG(pAd, fOP_AP_STATUS_MEDIA_STATE_CONNECTED);

	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("NDIS_STATUS_MEDIA_DISCONNECT Event B!\n"));

	rtmp_hif_data_init(pAd);

#ifdef SMART_CARRIER_SENSE_SUPPORT
#if 0
	/* Enable band0 IPI SW control */
        HW_IO_READ32(pAd, PHY_RXTD_12, &CrValue);
        CrValue |= (1 << 18);
        CrValue |= (1 << 29);
        HW_IO_WRITE32(pAd, PHY_RXTD_12, CrValue);
        HW_IO_WRITE32(pAd, PHY_RXTD_12, CrValue);
        /* Enable badn0 IPI control */
        HW_IO_READ32(pAd, PHY_BAND0_PHYMUX_5, &CrValue);
        CrValue |= (0x5 << 12);
        HW_IO_WRITE32(pAd, PHY_BAND0_PHYMUX_5, CrValue);  
#endif
	/* Enable Band0 PD_BLOCKING */
	HW_IO_READ32(pAd, PHY_MIN_PRI_PWR, &CrValue);
	CrValue |= (0x1 << PdBlkEnabeOffset); /* Bit[19] */
	HW_IO_WRITE32(pAd, PHY_MIN_PRI_PWR, CrValue);
	/* Enable Band1 PD_BLOCKING & initail PD_BLOCKING_TH */
	HW_IO_READ32(pAd, BAND1_PHY_MIN_PRI_PWR, &CrValue);
	CrValue |= (0x1 << PdBlkEnabeOffsetB1); /* Bit[25] */
	CrValue &= ~(PdBlkOfmdThMask << PdBlkOfmdThOffsetB1);  /* OFDM PD BLOCKING TH */
	CrValue |= (PdBlkOfmdThDefault <<PdBlkOfmdThOffsetB1);
	HW_IO_WRITE32(pAd, BAND1_PHY_MIN_PRI_PWR, CrValue);
#endif /* SMART_CARRIER_SENSE_SUPPORT */

#ifdef MAC_INIT_OFFLOAD
	AsicSetMacTxRx(pAd,ASIC_MAC_TXRX,TRUE);
#endif /*MAC_INIT_OFFLOAD*/

#ifdef CONFIG_AP_SUPPORT
	RT_CONFIG_IF_OPMODE_ON_AP(pAd->OpMode)
	{
		rtmp_ap_init(pAd);
	}
#endif
#ifdef CONFIG_STA_SUPPORT
    RT_CONFIG_IF_OPMODE_ON_STA(pAd->OpMode)
    {
		 rtmp_sta_init(pAd, &pAd->StaCfg[MAIN_MSTA_ID].wdev);
	}
#endif

#ifdef DYNAMIC_VGA_SUPPORT
	if (pAd->CommonCfg.lna_vga_ctl.bDyncVgaEnable)
	{
		dynamic_vga_enable(pAd);
	}
#endif /* DYNAMIC_VGA_SUPPORT */

	/* Set PHY to appropriate mode and will update the ChannelListNum in this function */

	if (pAd->ChannelListNum == 0)
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("Wrong configuration. No valid channel found. Check \"ContryCode\" and \"ChannelGeography\" setting.\n"));
		goto err3;
	}

#ifdef DOT11_N_SUPPORT
	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_OFF, ("MCS Set = %02x %02x %02x %02x %02x\n",
				pAd->CommonCfg.HtCapability.MCSSet[0],
				pAd->CommonCfg.HtCapability.MCSSet[1],
				pAd->CommonCfg.HtCapability.MCSSet[2],
				pAd->CommonCfg.HtCapability.MCSSet[3],
				pAd->CommonCfg.HtCapability.MCSSet[4]));
#endif /* DOT11_N_SUPPORT */

#ifdef UAPSD_SUPPORT
        UAPSD_Init(pAd);
#endif /* UAPSD_SUPPORT */

	/* assign function pointers*/
#ifdef MAT_SUPPORT
	/* init function pointers, used in OS_ABL */
	RTMP_MATOpsInit(pAd);
#endif /* MAT_SUPPORT */

#ifdef STREAM_MODE_SUPPORT
	AsicStreamModeInit(pAd);
#endif /* STREAM_MODE_SUPPORT */

#ifdef MT_WOW_SUPPORT
	ASIC_WOW_INIT(pAd);
#endif

#ifdef USB_IOT_WORKAROUND2
	pAd->bUSBIOTReady = TRUE;
#endif

#ifdef CONFIG_AP_SUPPORT
    AutoChSelInit(pAd);
#endif /* CONFIG_AP_SUPPORT */
    
#ifdef REDUCE_TCP_ACK_SUPPORT
    ReduceAckInit(pAd);
#endif
	/* Trigger MIB counter update */
	for (BandIdx = 0; BandIdx < DBDC_BAND_NUM; BandIdx++)
		pAd->OneSecMibBucket.Enabled[BandIdx] = TRUE;

	pAd->MsMibBucket.Enabled = TRUE;

	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_OFF, ("<==== mt_wifi_init, Status=%x\n", Status));

#if defined(TXBF_SUPPORT) && defined(MT_MAC) && (!defined(MT7636))
    mt_Trigger_Sounding_Packet(pAd,
                               TRUE,
	                              0,
	                  BF_PROCESSING,
	                              0,
	                           NULL);

    AsicTxBfHwEnStatusUpdate(pAd,
                             pAd->CommonCfg.ETxBfEnCond,
                             pAd->CommonCfg.RegTransmitSetting.field.ITxBfEn);
#endif /* TXBF_SUPPORT */

    /* EDCCA support */
    //EDCCACtrl = GetEDCCASupport(pAd);

    for (BandIdx = 0; BandIdx < DBDC_BAND_NUM; BandIdx++)
    {
        //pAd->CommonCfg.ucEDCCACtrl[BandIdx] = EDCCACtrl;    
        EDCCACtrlCmd(pAd, BandIdx, pAd->CommonCfg.ucEDCCACtrl[BandIdx]);
    }

#ifdef FTM_SUPPORT
	TmrCtrl(pAd, TMR_INITIATOR, pAd->chipCap.TmrHwVer);
#endif


#ifdef BACKGROUND_SCAN_SUPPORT
	BackgroundScanInit(pAd);

	/* Enable band0 IPI SW control */
        HW_IO_READ32(pAd, PHY_RXTD_12, &CrValue);
        CrValue |= (1 << B0IrpiSwCtrlResetOffset);
        CrValue |= (1 << B0IrpiSwCtrlOnlyOffset);
        HW_IO_WRITE32(pAd, PHY_RXTD_12, CrValue);
        HW_IO_WRITE32(pAd, PHY_RXTD_12, CrValue);
        /* Enable badn0 IPI control */
        HW_IO_READ32(pAd, PHY_BAND0_PHYMUX_5, &CrValue);
        CrValue |= (B0IpiEnableCtrlValue << B0IpiEnableCtrlOffset);
        HW_IO_WRITE32(pAd, PHY_BAND0_PHYMUX_5, CrValue);
#endif /* BACKGROUND_SCAN_SUPPORT */

#ifdef NR_PD_DETECTION
    if (pAd->CommonCfg.LinkTestSupport)
    {
        /* Enable 4T ACK Mechanism */
        pAd->fgWifiInitDone = TRUE;
    }
#endif /* NR_PD_DETECTION */

	return TRUE;
err3:
	MlmeHalt(pAd);
	RTMP_AllTimerListRelease(pAd);
err2:
	rtmp_sys_exit(pAd);
	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("!!! mt_wifi_init  fail !!!\n"));
	return FALSE;

}


VOID RTMPDrvOpen(VOID *pAdSrc)
{
	RTMP_ADAPTER *pAd = (RTMP_ADAPTER *)pAdSrc;
#ifdef CONFIG_STA_SUPPORT
    PSTA_ADMIN_CONFIG pStaCfg = &pAd->StaCfg[MAIN_MSTA_ID];
#endif

	RTMP_CLEAR_PSFLAG(pAd, fRTMP_PS_MCU_SLEEP);
#ifdef CONFIG_STA_SUPPORT
#ifdef DOT11R_FT_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
		FT_RIC_Init(pAd);
	}
#endif /* DOT11R_FT_SUPPORT */

#ifdef MT_MAC
#ifdef RT_CFG80211_SUPPORT
	CFG80211_InitTxSCallBack(pAd);
#endif /* RT_CFG80211_SUPPORT */
#endif /* MT_MAC */

#endif /* CONFIG_STA_SUPPORT */

#ifdef RTMP_MAC
	// TODO: shiang-usw, check this for RMTP_MAC
	if (pAd->chipCap.hif_type == HIF_RTMP) {
		/* Enable Interrupt*/
		RTMP_IRQ_ENABLE(pAd);

		/* Now Enable RxTx*/
		RTMPEnableRxTx(pAd);
	}
#endif /* RTMP_MAC */

#ifdef MT7601
	if ( IS_MT7601(pAd) )
		RTMPEnableRxTx(pAd);
#endif /* MT7601 */

	/*check all enabled function, decide the max unicast wtbl idx will use.*/
	HcSetMaxStaNum(pAd);
	RTMP_SET_FLAG(pAd, fRTMP_ADAPTER_START_UP);

#ifdef MT76XX_BTCOEX_SUPPORT
		// Init BT Coexistence
	if (IS_MT76XXBTCOMBO(pAd))
		InitBTCoexistence(pAd);
#endif /*MT76XX_BTCOEX_SUPPORT*/

#ifdef MT76x0
	if (IS_MT76x0(pAd))
	{
		/* Select Q2 to receive command response */
		andes_fun_set(pAd, Q_SELECT, pAd->chipCap.CmdRspRxRing);

#ifdef CONFIG_AP_SUPPORT
		IF_DEV_CONFIG_OPMODE_ON_AP(pAd)
		{
			AsicDisableSync(pAd, HW_BSSID_0);
			mt76x0_calibration(pAd, pAd->hw_cfg.cent_ch, TRUE, TRUE, TRUE);
			if (pAd->Dot11_H.RDMode != RD_SILENCE_MODE)
			AsicSetSyncModeAndEnable(pAd, pAd->CommonCfg.BeaconPeriod, HW_BSSID_0, OPMODE_AP);
		}
#endif /* CONFIG_AP_SUPPORT */
	}
#endif /* MT76x0 */

#if 0
	/*
	 * debugging helper
	 * 		show the size of main table in Adapter structure
	 *		MacTab  -- 185K
	 *		BATable -- 137K
	 * 		Total 	-- 385K  !!!!! (5/26/2006)
	 */
	printk("sizeof(pAd->MacTab) = %ld\n", sizeof(pAd->MacTab));
	printk("sizeof(pAd->AccessControlList) = %ld\n", sizeof(pAd->AccessControlList));
	printk("sizeof(pAd->ApCfg) = %ld\n", sizeof(pAd->ApCfg));
	printk("sizeof(pAd->BATable) = %ld\n", sizeof(pAd->BATable));
	BUG();
#endif

#ifdef CONFIG_STA_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
#ifdef PCIE_PS_SUPPORT
		RTMPInitPCIeLinkCtrlValue(pAd);
#endif /* PCIE_PS_SUPPORT */

#ifdef STA_EASY_CONFIG_SETUP
		pAd->Mlme.AutoProvisionMachine.CurrState = INFRA_AUTO_PROVISION;
		MlmeEnqueue(pAd, AUTO_PROVISION_STATE_MACHINE, MT2_CONN_AP, 0, NULL, 0);
		RTMP_MLME_HANDLER(pAd);
#endif /* STA_EASY_CONFIG_SETUP */
	}
#endif /* CONFIG_STA_SUPPORT */

#ifdef CONFIG_AP_SUPPORT
#ifdef BG_FT_SUPPORT
	BG_FTPH_Init();
#endif /* BG_FT_SUPPORT */
#endif /* CONFIG_AP_SUPPORT */

#ifdef CONFIG_STA_SUPPORT
	/*
		To reduce connection time,
		do auto reconnect here instead of waiting STAMlmePeriodicExec to do auto reconnect.
	*/
	if (pAd->OpMode == OPMODE_STA)
		MlmeAutoReconnectLastSSID(pAd, &pStaCfg->wdev);
#endif /* CONFIG_STA_SUPPORT */

#ifdef CONFIG_STA_SUPPORT
#ifdef DOT11W_PMF_SUPPORT
	if (pAd->OpMode == OPMODE_STA)
	{
		PMF_CFG *pPmfCfg = &pStaCfg->wdev.SecConfig.PmfCfg;
		pPmfCfg->MFPC = FALSE;
		pPmfCfg->MFPR = FALSE;
		pPmfCfg->PMFSHA256 = FALSE;

		if ((IS_AKM_WPA2_Entry(&pStaCfg->wdev) ||IS_AKM_WPA2PSK_Entry(&pStaCfg->wdev))
			&& IS_CIPHER_AES_Entry(&pStaCfg->wdev))
		{
			pPmfCfg->PMFSHA256 = pPmfCfg->Desired_PMFSHA256;
		
			if (pPmfCfg->Desired_MFPC)
			{
				pPmfCfg->MFPC = TRUE;
				pPmfCfg->MFPR = pPmfCfg->Desired_MFPR;
		
				if (pPmfCfg->MFPR)
					pPmfCfg->PMFSHA256 = TRUE;
			}
		}
		else if (pPmfCfg->Desired_MFPC)
		{
			MTWF_LOG(DBG_CAT_CFG, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("[PMF]%s:: Security is not WPA2/WPA2PSK AES\n", __FUNCTION__));
		}
		
		MTWF_LOG(DBG_CAT_CFG, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("[PMF]%s:: MFPC=%d, MFPR=%d, SHA256=%d\n",
					__FUNCTION__, pPmfCfg->MFPC, pPmfCfg->MFPR, pPmfCfg->PMFSHA256));
	}
#endif /* DOT11W_PMF_SUPPORT */
#endif /* CONFIG_STA_SUPPORT */

#ifdef WSC_INCLUDED
#ifdef CONFIG_AP_SUPPORT
#if 0
	if ((pAd->OpMode == OPMODE_AP)
#ifdef P2P_SUPPORT
		/* P2P will use ApCfg.MBSSID and ApCfg.ApCliTab also. */
		|| TRUE
#endif /* P2P_SUPPORT */
		)
	{
		INT index;
		for (index = 0; index < pAd->ApCfg.BssidNum; index++)
		{
#ifdef HOSTAPD_SUPPORT
			if (pAd->ApCfg.MBSSID[index].Hostapd == Hostapd_EXT)
			{
				MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("WPS is control by hostapd now.\n"));
			}
			else
#endif /*HOSTAPD_SUPPORT*/
			{
				PWSC_CTRL pWscControl;
				UCHAR zeros16[16]= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

				pWscControl = &pAd->ApCfg.MBSSID[index].WscControl;
				MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("Generate UUID for apidx(%d)\n", index));
				if (NdisEqualMemory(&pWscControl->Wsc_Uuid_E[0], zeros16, UUID_LEN_HEX))
					WscGenerateUUID(pAd, &pWscControl->Wsc_Uuid_E[0], &pWscControl->Wsc_Uuid_Str[0], index, FALSE);
				WscInit(pAd, FALSE, index);
			}
		}

#ifdef APCLI_SUPPORT
		for(index = 0; index < MAX_APCLI_NUM; index++)
		{
			PWSC_CTRL pWpsCtrl = &pAd->ApCfg.ApCliTab[index].WscControl;

			pWpsCtrl->pAd = pAd;
			NdisZeroMemory(pWpsCtrl->EntryAddr, MAC_ADDR_LEN);
#ifdef WSC_V2_SUPPORT
			pWpsCtrl->WscConfigMethods= 0x238C;
#else /* WSC_V2_SUPPORT */
			pWpsCtrl->WscConfigMethods= 0x018C;
#endif /* !WSC_V2_SUPPORT */
			RTMP_AP_IoctlHandle(pAd, NULL, CMD_RTPRIV_IOCTL_WSC_INIT, 0, (VOID *)&pAd->ApCfg.ApCliTab[index], index);
		}
#endif /* APCLI_SUPPORT */
	}
#endif
#endif /* CONFIG_AP_SUPPORT */

#ifdef CONFIG_STA_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
		PWSC_CTRL pWscControl = &pStaCfg->WscControl;

		WscGenerateUUID(pAd, &pWscControl->Wsc_Uuid_E[0], &pWscControl->Wsc_Uuid_Str[0], 0, FALSE, FALSE);
		WscInit(pAd, FALSE, BSS0);
#ifdef WSC_V2_SUPPORT
		WscInitRegistrarPair(pAd, &pStaCfg->WscControl, BSS0);
#endif /* WSC_V2_SUPPORT */
	}
#endif /* CONFIG_STA_SUPPORT */

	/* WSC hardware push button function 0811 */
	WSC_HDR_BTN_Init(pAd);
#endif /* WSC_INCLUDED */
#ifdef MT76XX_BTCOEX_SUPPORT
	//SendAndesWLANStatus(pAd,WLAN_Device_ON,0);
	if (IS_MT76XXBTCOMBO(pAd))
		MLMEHook(pAd, WLAN_Device_ON, 0);
#endif /*MT76XX_BTCOEX_SUPPORT*/

#ifdef MT_MAC_BTCOEX
	//SendAndesWLANStatus(pAd,WLAN_Device_ON,0);
	if (IS_MT76x6(pAd)||IS_MT7637(pAd))
		MT7636MLMEHook(pAd, MT7636_WLAN_Device_ON, 0);
#endif /*MT_MAC_BTCOEX*/

#ifdef MT_WOW_SUPPORT
	pAd->WOW_Cfg.bWoWRunning = FALSE;
#endif

#ifdef CONFIG_AP_SUPPORT
#ifdef VOW_SUPPORT
    if (IS_MT7615(pAd) || IS_MT7622(pAd))
        vow_init(pAd);
#else
    vow_atf_off_init(pAd);
#endif /* VOW_SUPPORT */
#endif /* CONFIG_AP_SUPPORT */

#ifdef RED_SUPPORT
	if(pAd->OpMode == OPMODE_AP)
		red_is_enabled(pAd);
#endif /* RED_SUPPORT */

	cp_support_is_enabled(pAd);

#if defined(MT_DFS_SUPPORT) && defined(BACKGROUND_SCAN_SUPPORT)
	if (IS_MT7615(pAd)) {
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("Trigger DFS Zero wait procedure Support=%d, DfsZeroWaitChannel=%d", pAd->BgndScanCtrl.DfsZeroWaitSupport, pAd->BgndScanCtrl.DfsZeroWaitChannel));
		if (pAd->BgndScanCtrl.DfsZeroWaitSupport == 1 && pAd->BgndScanCtrl.DfsZeroWaitChannel !=0)
			DfsZeroWaitStart(pAd, TRUE);
	}
#endif /* defined(MT_DFS_SUPPORT) && defined(BACKGROUND_SCAN_SUPPORT) */

}


VOID RTMPDrvClose(VOID *pAdSrc, VOID *net_dev)
{
	RTMP_ADAPTER *pAd = (RTMP_ADAPTER *)pAdSrc;
	UINT32 i = 0;
	struct MCU_CTRL *prCtl = NULL;
#ifdef RTMP_SDIO_SUPPORT
	UINT32 count = 0;
#endif /* RTMP_SDIO_SUPPORT */
#ifdef CONFIG_STA_SUPPORT
	BOOLEAN	InWOW = FALSE;
	PSTA_ADMIN_CONFIG pStaCfg = NULL;
#endif /* CONFIG_STA_SUPPORT */
	prCtl = &pAd->MCUCtrl;

#ifdef CONFIG_STA_SUPPORT

#ifdef MT_MAC_BTCOEX
	if (IS_MT76x6(pAd)||IS_MT7637(pAd))
		MT7636MLMEHook(pAd, MT7636_WLAN_Device_OFF, 0);
#endif /*MT76XX_BTCOEX_SUPPORT*/

#ifdef CREDENTIAL_STORE
		if (pAd->IndicateMediaState == NdisMediaStateConnected)
			StoreConnectInfo(pAd);
		else
		{
			RTMP_SEM_LOCK(&pAd->StaCtIf.Lock);
			pAd->StaCtIf.Changeable = FALSE;
			RTMP_SEM_UNLOCK(&pAd->StaCtIf.Lock);
		}
#endif /* CREDENTIAL_STORE */
#endif /* CONFIG_STA_SUPPORT */

#ifdef CONFIG_AP_SUPPORT
#ifdef BG_FT_SUPPORT
	BG_FTPH_Remove();
#endif /* BG_FT_SUPPORT */
#endif /* CONFIG_AP_SUPPORT */

#ifdef RTMP_RBUS_SUPPORT
#ifdef RT3XXX_ANTENNA_DIVERSITY_SUPPORT
	if (pAd->infType == RTMP_DEV_INF_RBUS)
	RT3XXX_AntDiversity_Fini(pAd);
#endif /* RT3XXX_ANTENNA_DIVERSITY_SUPPORT */
#endif /* RTMP_RBUS_SUPPORT */

	RTMP_SET_FLAG(pAd, fRTMP_ADAPTER_POLL_IDLE);

#ifdef CONFIG_STA_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
#ifdef PCIE_PS_SUPPORT
		RTMPPCIeLinkCtrlValueRestore(pAd, RESTORE_CLOSE);
#endif /* PCIE_PS_SUPPORT */

		/* If dirver doesn't wake up firmware here,*/
		/* NICLoadFirmware will hang forever when interface is up again.*/
            for (i = 0; i < MAX_MULTI_STA; i++) {
                pStaCfg = &pAd->StaCfg[i];
                if (INFRA_ON(pStaCfg) && pStaCfg->PwrMgmt.bDoze) {
                    RTMP_FORCE_WAKEUP(pAd, pStaCfg);
                }
            }

#ifdef RTMP_MAC_USB
		RTMP_SET_FLAG(pAd, fRTMP_ADAPTER_REMOVE_IN_PROGRESS);
#endif /* RTMP_MAC_USB */

#ifdef RTMP_MAC_PCI
		pAd->bPCIclkOff = FALSE;
#endif /* RTMP_MAC_PCI */
	}
#endif /* CONFIG_STA_SUPPORT */

#ifdef MT_MAC
	if (pAd->chipCap.hif_type != HIF_MT)
#endif /* MT_MAC */
		RTMP_SET_FLAG(pAd, fRTMP_ADAPTER_HALT_IN_PROGRESS);

#ifdef EXT_BUILD_CHANNEL_LIST
	if (pAd->CommonCfg.pChDesp != NULL)
		os_free_mem(pAd->CommonCfg.pChDesp);
	pAd->CommonCfg.pChDesp = NULL;
	pAd->CommonCfg.DfsType = MAX_RD_REGION;
	pAd->CommonCfg.bCountryFlag = 0;
#endif /* EXT_BUILD_CHANNEL_LIST */
	pAd->CommonCfg.bCountryFlag = FALSE;

#ifdef WMM_ACM_SUPPORT
	/* must call first */
	ACMP_Release(pAd);
#endif /* WMM_ACM_SUPPORT */

#ifdef MESH_SUPPORT
	/* close all mesh link before the interface go down.*/
	if (MESH_ON(pAd))
		MeshDown(pAd, TRUE);
#endif /* MESH_SUPPORT */

	for (i = 0 ; i < NUM_OF_TX_RING; i++)
	{
		while (pAd->DeQueueRunning[i] == TRUE)
		{
			MTWF_LOG(DBG_CAT_ALL, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("Waiting for TxQueue[%d] done..........\n", i));
			RtmpusecDelay(1000);
		}
	}

#ifdef CONFIG_AP_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_AP(pAd)
	{
		BOOLEAN Cancelled = FALSE;
#if defined (RTMP_MAC_USB) || defined (RTMP_MAC_SDIO)
		RTMPCancelTimer(&pAd->CommonCfg.BeaconUpdateTimer, &Cancelled);
#endif /* RTMP_MAC_USB */

#ifdef DOT11N_DRAFT3
		if (pAd->CommonCfg.Bss2040CoexistFlag & BSS_2040_COEXIST_TIMER_FIRED)
		{
			RTMPCancelTimer(&pAd->CommonCfg.Bss2040CoexistTimer, &Cancelled);
			pAd->CommonCfg.Bss2040CoexistFlag  = 0;
		}
#endif /* DOT11N_DRAFT3 */
	}
#endif /* CONFIG_AP_SUPPORT */

#ifdef CONFIG_STA_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
		MacTableReset(pAd);
#ifdef MAT_SUPPORT
		MATEngineExit(pAd);
#endif /* MAT_SUPPORT */
	}
#endif /* CONFIG_STA_SUPPORT */

#ifdef CONFIG_AP_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_AP(pAd)
	{
#ifdef MAT_SUPPORT
		MATEngineExit(pAd);
#endif /* MAT_SUPPORT */

#ifdef CLIENT_WDS
		CliWds_ProxyTabDestory(pAd);
#endif /* CLIENT_WDS */
		/* Shutdown Access Point function, release all related resources */
		APShutdown(pAd);

/*#ifdef AUTO_CH_SELECT_ENHANCE*/
		/* Free BssTab & ChannelInfo tabbles.*/
/*		AutoChBssTableDestroy(pAd); */
/*		ChannelInfoDestroy(pAd); */
/*#endif  AUTO_CH_SELECT_ENHANCE */
	}
#endif /* CONFIG_AP_SUPPORT */

#ifdef CONFIG_ATE
    ATEExit(pAd);
#endif /*CONFIG_ATE*/

	/* Stop Mlme state machine*/
	MlmeHalt(pAd);                

	/* Close net tasklets*/
#ifdef CONFIG_STA_SUPPORT	
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd){
#if (defined(WOW_SUPPORT) && defined(RTMP_MAC_USB)) || defined(NEW_WOW_SUPPORT) || defined(MT_WOW_SUPPORT)
		for (i = 0; i < MAX_MULTI_STA; i++) {
			pStaCfg = &pAd->StaCfg[i];
			if ((pAd->WOW_Cfg.bEnable) && 
				(pAd->WOW_Cfg.bWowIfDownSupport) && 
				INFRA_ON(pStaCfg)) {
				InWOW = TRUE;
				break;
			}
		}
#endif /* WOW */		
	}
	if (!InWOW)
#endif /* CONFIG_STA_SUPPORT */
	NICRestartFirmware(pAd);

#ifdef RTMP_USB_SUPPORT
	/*  Polling TX/RX path until packets empty */
	MTUsbPollTxRxEmpty(pAd);
#endif

	/*  Disable Interrupt */
	if (IS_MT7637(pAd)) //workaround for MtCmdRestartDLReq
	{
#ifdef RTMP_PCI_SUPPORT
		/*  Polling TX/RX path until packets empty */
		MTPciPollTxRxEmpty(pAd);
		/*  Disable PDMA */
		AsicSetWPDMA(pAd, PDMA_TX_RX, 0);
		/*  Polling TX/RX path until packets empty */
		MTPciPollTxRxEmpty(pAd);
#endif
		
		HcSetAllSupportedBandsRadioOff(pAd);

		if (RTMP_TEST_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_ACTIVE))
		{
			RTMP_ASIC_INTERRUPT_DISABLE(pAd);
		}
	}

#ifdef  RTMP_SDIO_SUPPORT
	RTMP_CLEAR_FLAG(pAd, fRTMP_ADAPTER_MCU_SEND_IN_BAND_CMD);

	count=0;
	while (AndesQueueLen(prCtl, &prCtl->txq_sdio) > 0) {
		if(count>1000){
			MTWF_LOG(DBG_CAT_ALL, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("%s AndesQueueLen(prCtl, &prCtl->txq_sdio) > 0\n",__FUNCTION__));
			break;
		}
		count++;
		RtmpOsMsDelay(1);
	}
	count=0;
	while (AndesQueueLen(prCtl, &prCtl->ackq) > 0) {
		if(count>1000){
			MTWF_LOG(DBG_CAT_ALL, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("%s AndesQueueLen(prCtl, &prCtl->ackq) > 0\n",__FUNCTION__));
			break;
		}
		count++;

		RtmpOsMsDelay(1);
	}
	count=0;
	while (AndesQueueLen(prCtl, &prCtl->kickq) > 0) {

		if(count>1000){
			MTWF_LOG(DBG_CAT_ALL, DBG_SUBCAT_ALL, DBG_LVL_ERROR, ("%s AndesQueueLen(prCtl, &prCtl->kickq) > 0\n",__FUNCTION__));
			break;
		}
		count++;
		RtmpOsMsDelay(1);
	}

#endif
	MeasureReqTabExit(pAd);
	TpcReqTabExit(pAd);

#ifdef LED_CONTROL_SUPPORT
	RTMPExitLEDMode(pAd);
#endif // LED_CONTROL_SUPPORT

	/* Close kernel threads*/
	RtmpMgmtTaskExit(pAd);

#ifdef RTMP_MAC_PCI
	{
#if defined(MT76x0) || defined(MT76x2)
		if (IS_MT76x0(pAd) || IS_MT76x2(pAd)) {
			DISABLE_TX_RX(pAd, RTMP_HALT);
		}
		else
#endif /* MT76x0 */
		{
			if (RTMP_TEST_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_ACTIVE))
			{
				DISABLE_TX_RX(pAd, RTMP_HALT);
				RTMP_ASIC_INTERRUPT_DISABLE(pAd);
			}
		}

		/* Receive packets to clear DMA index after disable interrupt. */
		/*RTMPHandleRxDoneInterrupt(pAd);*/
		/* put to radio off to save power when driver unload.  After radiooff, can't write /read register.  So need to finish all */
		/* register access before Radio off.*/

#ifdef RTMP_PCI_SUPPORT
		if (pAd->infType == RTMP_DEV_INF_PCI || pAd->infType == RTMP_DEV_INF_PCIE)
		{
			BOOLEAN brc = TRUE;

#if defined(MT76x0) || defined(MT76x2)
			// TODO: shiang, how about RadioOff for 65xx??
			if (!(IS_MT76x0(pAd) || IS_MT76x2(pAd)))
#endif /* defined(MT76x0) || defined(MT76x2) */
				brc = RT28xxPciAsicRadioOff(pAd, RTMP_HALT, 0);

/*In  solution 3 of 3090F, the bPCIclkOff will be set to TRUE after calling RT28xxPciAsicRadioOff*/
#ifdef PCIE_PS_SUPPORT
			pAd->bPCIclkOff = FALSE;
#endif /* PCIE_PS_SUPPORT */

			if (brc==FALSE)
			{
				MTWF_LOG(DBG_CAT_ALL, DBG_SUBCAT_ALL, DBG_LVL_ERROR,("%s call RT28xxPciAsicRadioOff fail !!\n", __FUNCTION__));
			}
		}
#endif /* RTMP_PCI_SUPPORT */
	}

#endif /* RTMP_MAC_PCI */

	/* Free IRQ*/
	if (RTMP_TEST_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_REGISTER_TO_OS))
	{
#ifdef RTMP_MAC_PCI
		/* Deregister interrupt function*/
		RTMP_OS_IRQ_RELEASE(pAd, net_dev);
#endif /* RTMP_MAC_PCI */
		RTMP_CLEAR_FLAG(pAd, fRTMP_ADAPTER_INTERRUPT_REGISTER_TO_OS);
	}

#ifdef SINGLE_SKU_V2
	RTMPResetSingleSKUParameters(pAd);
    RTMPResetBfBackOffTable(pAd);
#endif

	/*remove hw related system info*/
	WfSysPosExit(pAd);

	/* Free FW file allocated buffer */
	NICEraseFirmware(pAd);

	RTMP_CLEAR_FLAG(pAd, fRTMP_ADAPTER_HALT_IN_PROGRESS);

#ifdef WLAN_SKB_RECYCLE
	skb_queue_purge(&pAd->rx0_recycle);
#endif /* WLAN_SKB_RECYCLE */

#ifdef DOT11_N_SUPPORT
	/* Free BA reorder resource*/
	ba_reordering_resource_release(pAd);
#endif /* DOT11_N_SUPPORT */

	UserCfgExit(pAd); /* must after ba_reordering_resource_release */

#ifdef MT_MAC
	if (pAd->chipCap.hif_type == HIF_MT)
		ExitTxSTypeTable(pAd);
#endif

#ifdef CONFIG_STA_SUPPORT
#ifdef DOT11R_FT_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
		FT_RIC_Release(pAd);
	}
#endif /* DOT11R_FT_SUPPORT */
#endif /* CONFIG_STA_SUPPORT */

#ifdef BACKGROUND_SCAN_SUPPORT
    BackgroundScanDeInit(pAd); 
#endif /* BACKGROUND_SCAN_SUPPORT */

#ifdef CONFIG_AP_SUPPORT
    AutoChSelRelease(pAd);    
#endif/* CONFIG_AP_SUPPORT */

	RTMP_CLEAR_FLAG(pAd, fRTMP_ADAPTER_START_UP);

/*+++Modify by woody to solve the bulk fail+++*/
#ifdef CONFIG_STA_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
#ifdef RT35xx
		if (IS_RT3572(pAd))
		{
			RT30xxWriteRFRegister(pAd, RF_R08, 0x00);
			AsicSendCommandToMcu(pAd, 0x30, 0xff, 0xff, 0x02, FALSE);
		}
#endif /* RT35xx */
#ifdef DOT11Z_TDLS_SUPPORT
		TDLS_Table_Destory(pAd);
#ifdef TDLS_AUTOLINK_SUPPORT
		TDLS_ClearEntryList(&pStaCfg->TdlsInfo.TdlsDiscovPeerList);
		NdisFreeSpinLock(&pStaCfg->TdlsInfo.TdlsDiscovPeerListSemLock);
		TDLS_ClearEntryList(&pStaCfg->TdlsInfo.TdlsBlackList);
		NdisFreeSpinLock(&pStaCfg->TdlsInfo.TdlsBlackListSemLock);
#endif /* TDLS_AUTOLINK_SUPPORT */
#endif /* DOT11Z_TDLS_SUPPORT */
	}
#endif /* CONFIG_STA_SUPPORT */

	/* clear MAC table */
	/* TODO: do not clear spin lock, such as fLastChangeAccordingMfbLock */
	NdisZeroMemory(&pAd->MacTab, sizeof(MAC_TABLE));

	/* release all timers */
	RtmpusecDelay(2000);
	RTMP_AllTimerListRelease(pAd);

	/* WCNCR00034259: moved from RTMP{Reset, free}TxRxRingMemory() */
	NdisFreeSpinLock(&pAd->CmdQLock);

#ifdef RTMP_TIMER_TASK_SUPPORT
	NdisFreeSpinLock(&pAd->TimerQLock);
#endif /* RTMP_TIMER_TASK_SUPPORT */

#ifdef CONFIG_FPGA_MODE
#ifdef CAPTURE_MODE
	cap_mode_deinit(pAd);
#endif /* CAPTURE_MODE */
#endif /* CONFIG_FPGA_MODE */
#ifdef RTMP_SDIO_SUPPORT
//	RTMP_SDIO_WRITE32(pAd, WHLPCR, W_FW_OWN_REQ_SET);
	MakeFWOwn(pAd);
#endif
#ifdef CONFIG_FWOWN_SUPPORT
	FwOwn(pAd);
#endif /* CONFIG_FWOWN_SUPPORT */
	/* Close Hw ctrl*/
	HwCtrlExit(pAd);

#ifdef REDUCE_TCP_ACK_SUPPORT
	ReduceAckExit(pAd);
#endif

#ifdef PRE_CAL_TRX_SET1_SUPPORT
	if(pAd->E2pAccessMode == E2P_FLASH_MODE || pAd->E2pAccessMode == E2P_BIN_MODE)
	{		
		if(pAd->CalDCOCImage != NULL)
			os_free_mem(pAd->CalDCOCImage);
		if(pAd->CalDPDAPart1Image != NULL)
			os_free_mem(pAd->CalDPDAPart1Image);
		if(pAd->CalDPDAPart2GImage != NULL)
			os_free_mem(pAd->CalDPDAPart2GImage);
	}
#endif /* PRE_CAL_TRX_SET1_SUPPORT */
#ifdef PRE_CAL_TRX_SET2_SUPPORT
	if(pAd->PreCalStoreBuffer != NULL)
	{
		os_free_mem(pAd->PreCalStoreBuffer);
		pAd->PreCalStoreBuffer = NULL;
	}
	if(pAd->PreCalReStoreBuffer != NULL)
	{
		os_free_mem(pAd->PreCalReStoreBuffer);    
		pAd->PreCalReStoreBuffer = NULL;
	}
#endif/* PRE_CAL_TRX_SET2_SUPPORT */    
/*multi profile release*/
#ifdef MULTI_PROFILE
	multi_profile_exit(pAd);
#endif /*MULTI_PROFILE*/

#ifdef NF_SUPPORT
	pAd->Avg_NF[DBDC_BAND0] = pAd->Avg_NFx16[DBDC_BAND0] = 0;
	if(pAd->CommonCfg.dbdc_mode)
		pAd->Avg_NF[DBDC_BAND1] = pAd->Avg_NFx16[DBDC_BAND1] = 0;
	 NdisFreeSpinLock(&pAd->ScanCtrl.NF_Lock);
#endif
}


VOID RTMPInfClose(VOID *pAdSrc)
{
	RTMP_ADAPTER *pAd = (RTMP_ADAPTER *)pAdSrc;
	struct wifi_dev *wdev;
#ifdef CONFIG_STA_SUPPORT
    PSTA_ADMIN_CONFIG pStaCfg = &pAd->StaCfg[MAIN_MSTA_ID];

    wdev = &pStaCfg->wdev;
#endif

#ifdef CONFIG_AP_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_AP(pAd)
	{
		wdev = &pAd->ApCfg.MBSSID[MAIN_MBSSID].wdev;
		wdev->bAllowBeaconing = FALSE;
		WifiSysApLinkDown(pAd,wdev);
		WifiSysClose(pAd,wdev);
	}
#endif /*CONFIG_AP_SUPPROT*/

#ifdef CONFIG_STA_SUPPORT
	IF_DEV_CONFIG_OPMODE_ON_STA(pAd)
	{
#ifdef PROFILE_STORE
		WriteDatThread(pAd);
		RtmpusecDelay(1000);
#endif /* PROFILE_STORE */

		if (INFRA_ON(pStaCfg) &&
			(!RTMP_TEST_FLAG(pAd, fRTMP_ADAPTER_NIC_NOT_EXIST)))
        {
#ifdef MT_WOW_SUPPORT
            if (pAd->WOW_Cfg.bEnable && pAd->WOW_Cfg.bWowIfDownSupport)
            {
                AsicSwitchChannel(pAd, wdev->channel, FALSE);
                pAd->WOW_Cfg.bWoWRunning = TRUE;
                ASIC_WOW_ENABLE(pAd, pStaCfg);
                AsicExtPmStateCtrl(pAd, pStaCfg, PM4, ENTER_PM_STATE);
            }
            else
            {
                MSTAStop(pAd, wdev);
            }
#else
            MSTAStop(pAd, wdev);
#endif
        }

        WifiSysClose(pAd,wdev);

#ifdef WPA_SUPPLICANT_SUPPORT
#ifndef NATIVE_WPA_SUPPLICANT_SUPPORT
		/* send wireless event to wpa_supplicant for infroming interface down.*/
		RtmpOSWrielessEventSend(pAd->net_dev, RT_WLAN_EVENT_CUSTOM, RT_INTERFACE_DOWN, NULL, NULL, 0);
#endif /* NATIVE_WPA_SUPPLICANT_SUPPORT */

		if (pStaCfg->wpa_supplicant_info.pWpsProbeReqIe)
		{
			os_free_mem(pStaCfg->wpa_supplicant_info.pWpsProbeReqIe);
			pStaCfg->wpa_supplicant_info.pWpsProbeReqIe = NULL;
			pStaCfg->wpa_supplicant_info.WpsProbeReqIeLen = 0;
		}

		if (pStaCfg->wpa_supplicant_info.pWpaAssocIe)
		{
			os_free_mem(pStaCfg->wpa_supplicant_info.pWpaAssocIe);
			pStaCfg->wpa_supplicant_info.pWpaAssocIe = NULL;
			pStaCfg->wpa_supplicant_info.WpaAssocIeLen = 0;
		}
#endif /* WPA_SUPPLICANT_SUPPORT */
	}
#endif /* CONFIG_STA_SUPPORT */

#ifdef RT_CFG80211_SUPPORT			
	pAd->cfg80211_ctrl.beaconIsSetFromHostapd = FALSE;
#endif

}


PNET_DEV RtmpPhyNetDevMainCreate(VOID *pAdSrc)
{
	RTMP_ADAPTER *pAd = (RTMP_ADAPTER *)pAdSrc;
	PNET_DEV pDevNew;
	UINT32 MC_RowID = 0, IoctlIF = 0;
	char *dev_name;

#ifdef MULTIPLE_CARD_SUPPORT
	MC_RowID = pAd->MC_RowID;
#endif /* MULTIPLE_CARD_SUPPORT */
#ifdef HOSTAPD_SUPPORT
	IoctlIF = pAd->IoctlIF;
#endif /* HOSTAPD_SUPPORT */

	dev_name = get_dev_name_prefix(pAd, INT_MAIN);
#ifdef INTELP6_SUPPORT
#ifdef MT_SECOND_CARD
	if (pAd->dev_idx == 1)
		pDevNew = RtmpOSNetDevCreate((INT32)MC_RowID, (UINT32 *)&IoctlIF,
					INT_MAIN, MAX_MBSS_NUM, sizeof(struct mt_dev_priv), dev_name);
	else
#endif
#endif
	pDevNew = RtmpOSNetDevCreate((INT32)MC_RowID, (UINT32 *)&IoctlIF,
					INT_MAIN, 0, sizeof(struct mt_dev_priv), dev_name);

#ifdef HOSTAPD_SUPPORT
	pAd->IoctlIF = IoctlIF;
#endif /* HOSTAPD_SUPPORT */

	return pDevNew;
}


#ifdef CONFIG_STA_SUPPORT
#ifdef PROFILE_STORE
static void WriteConfToDatFile(RTMP_ADAPTER *pAd)
{
	char	*cfgData = 0, *offset = 0;
	RTMP_STRING *fileName = NULL, *pTempStr = NULL;
	RTMP_OS_FD file_r, file_w;
	RTMP_OS_FS_INFO osFSInfo;
	LONG rv, fileLen = 0;


	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("-----> WriteConfToDatFile\n"));

#ifdef RTMP_RBUS_SUPPORT
	if (pAd->infType == RTMP_DEV_INF_RBUS)
		fileName = STA_PROFILE_PATH_RBUS;
	else
#endif /* RTMP_RBUS_SUPPORT */
		fileName = STA_PROFILE_PATH;

	RtmpOSFSInfoChange(&osFSInfo, TRUE);

	file_r = RtmpOSFileOpen(fileName, O_RDONLY, 0);
	if (IS_FILE_OPEN_ERR(file_r))
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("-->1) %s: Error opening file %s\n", __FUNCTION__, fileName));
		return;
	}
	else
	{
		char tempStr[64] = {0};
		while((rv = RtmpOSFileRead(file_r, tempStr, 64)) > 0)
		{
			fileLen += rv;
		}
		os_alloc_mem(NULL, (UCHAR **)&cfgData, fileLen);
		if (cfgData == NULL)
		{
			RtmpOSFileClose(file_r);
			MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("CfgData mem alloc fail. (fileLen = %ld)\n", fileLen));
			goto out;
		}
		NdisZeroMemory(cfgData, fileLen);
		RtmpOSFileSeek(file_r, 0);
		rv = RtmpOSFileRead(file_r, (RTMP_STRING *)cfgData, fileLen);
		RtmpOSFileClose(file_r);
		if (rv != fileLen)
		{
			MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("CfgData mem alloc fail, fileLen = %ld\n", fileLen));
			goto ReadErr;
		}
	}

	file_w = RtmpOSFileOpen(fileName, O_WRONLY|O_TRUNC, 0);
	if (IS_FILE_OPEN_ERR(file_w))
	{
		goto WriteFileOpenErr;
	}
	else
	{
		offset = (PCHAR) rtstrstr((RTMP_STRING *) cfgData, "Default\n");
		offset += strlen("Default\n");
		RtmpOSFileWrite(file_w, (RTMP_STRING *)cfgData, (int)(offset-cfgData));
		os_alloc_mem(NULL, (UCHAR **)&pTempStr, 512);
		if (!pTempStr)
		{
			MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("pTempStr mem alloc fail. (512)\n"));
			RtmpOSFileClose(file_w);
			goto WriteErr;
		}

		for (;;)
		{
			int i = 0;
			RTMP_STRING *ptr;

			NdisZeroMemory(pTempStr, 512);
			ptr = (RTMP_STRING *) offset;
			while(*ptr && *ptr != '\n')
			{
				pTempStr[i++] = *ptr++;
			}
			pTempStr[i] = 0x00;
			if ((size_t)(offset - cfgData) < fileLen)
			{
				offset += strlen(pTempStr) + 1;
				if (strncmp(pTempStr, "SSID=", strlen("SSID=")) == 0)
				{
					NdisZeroMemory(pTempStr, 512);
					NdisMoveMemory(pTempStr, "SSID=", strlen("SSID="));
					NdisMoveMemory(pTempStr + 5, pAd->CommonCfg.Ssid, pAd->CommonCfg.SsidLen);
				}
				else if (strncmp(pTempStr, "AuthMode=", strlen("AuthMode=")) == 0)
				{
					struct _SECURITY_CONFIG *pSecConfig = &pStaCfg->wdev.SecConfig;
					NdisZeroMemory(pTempStr, 512);
					if (IS_AKM_OPEN(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=OPEN");
					else if (IS_AKM_SHARED(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=SHARED");
					else if (IS_AKM_AUTOSWITCH(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=WEPAUTO");
					else if (IS_AKM_WPA1PSK(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=WPAPSK");
					else if (IS_AKM_WPA2PSK(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=WPA2PSK");
					else if (IS_AKM_WPA1(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=WPA");
					else if (IS_AKM_WPA2(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=WPA2");
					else if (IS_AKM_WPANONE(pSecConfig->AKMMap))
						sprintf(pTempStr, "AuthMode=WPANONE");
				}
				else if (strncmp(pTempStr, "EncrypType=", strlen("EncrypType=")) == 0)
				{
					struct _SECURITY_CONFIG *pSecConfig = &pAd->StaCfg[0].wdev.SecConfig;
					NdisZeroMemory(pTempStr, 512);
					if (IS_CIPHER_NONE(pSecConfig->PairwiseCipher))
						sprintf(pTempStr, "EncrypType=NONE");
					else if (IS_CIPHER_WEP(pSecConfig->PairwiseCipher))
						sprintf(pTempStr, "EncrypType=WEP");
					else if (IS_CIPHER_TKIP(pSecConfig->PairwiseCipher))
						sprintf(pTempStr, "EncrypType=TKIP");
					else if (IS_CIPHER_CCMP128(pSecConfig->PairwiseCipher))
						sprintf(pTempStr, "EncrypType=AES");
					else if (IS_CIPHER_CCMP256(pSecConfig->PairwiseCipher))
						sprintf(pTempStr, "EncrypType=CCMP256");
					else if (IS_CIPHER_GCMP128(pSecConfig->PairwiseCipher))
						sprintf(pTempStr, "EncrypType=GCMP128");
					else if (IS_CIPHER_GCMP256(pSecConfig->PairwiseCipher))
						sprintf(pTempStr, "EncrypType=GCMP256");
				}
				RtmpOSFileWrite(file_w, pTempStr, strlen(pTempStr));
				RtmpOSFileWrite(file_w, "\n", 1);
			}
			else
			{
				break;
			}
		}
		RtmpOSFileClose(file_w);
	}

WriteErr:
	if (pTempStr)
		os_free_mem(pTempStr);
ReadErr:
WriteFileOpenErr:
	if (cfgData)
		os_free_mem(cfgData);
out:
	RtmpOSFSInfoChange(&osFSInfo, FALSE);


	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("<----- WriteConfToDatFile\n"));
	return;
}


INT write_dat_file_thread (
    IN ULONG Context)
{
	RTMP_OS_TASK *pTask;
	RTMP_ADAPTER *pAd;
	//int 	Status = 0;

	pTask = (RTMP_OS_TASK *)Context;

	if (pTask == NULL)
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("%s: pTask is NULL\n", __FUNCTION__));
		return 0;
	}

	pAd = (PRTMP_ADAPTER)RTMP_OS_TASK_DATA_GET(pTask);

	if (pAd == NULL)
	{
		MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("%s: pAd is NULL\n", __FUNCTION__));
		return 0;
	}

	RtmpOSTaskCustomize(pTask);

	/* Update ssid, auth mode and encr type to DAT file */
	WriteConfToDatFile(pAd);

		RtmpOSTaskNotifyToExit(pTask);

	return 0;
}

NDIS_STATUS WriteDatThread(
	IN  RTMP_ADAPTER *pAd)
{
	NDIS_STATUS status = NDIS_STATUS_FAILURE;
	RTMP_OS_TASK *pTask;

	if (pAd->bWriteDat == FALSE)
		return 0;

	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("-->WriteDatThreadInit()\n"));

	pTask = &pAd->WriteDatTask;

	RTMP_OS_TASK_INIT(pTask, "RtmpWriteDatTask", pAd);
	status = RtmpOSTaskAttach(pTask, write_dat_file_thread, (ULONG)&pAd->WriteDatTask);
	MTWF_LOG(DBG_CAT_INIT, DBG_SUBCAT_ALL, DBG_LVL_TRACE, ("<--WriteDatThreadInit(), status=%d!\n", status));

	return status;
}
#endif /* PROFILE_STORE */
#endif /* CONFIG_STA_SUPPORT */

#ifdef ERR_RECOVERY
INT	Set_ErrDetectOn_Proc(
    IN PRTMP_ADAPTER pAd,
    IN RTMP_STRING *arg)
{
	UINT32 Enable;

	Enable = simple_strtol(arg, 0, 10);
    CmdExtGeneralTestOn(pAd, (Enable == 0) ? (FALSE) : (TRUE));

    return TRUE;
}

INT	Set_ErrDetectMode_Proc(
    IN PRTMP_ADAPTER pAd,
    IN RTMP_STRING *arg)
{
    UINT8 mode = 0;
    UINT8 sub_mode = 0;
    PCHAR seg_str;

    if ((seg_str = strsep((char **)&arg, "_")) != NULL)
    {
        mode = (BOOLEAN) simple_strtol(seg_str, 0, 10);
    }

    if ((seg_str = strsep((char **)&arg, "_")) != NULL)
    {
        sub_mode = (BOOLEAN) simple_strtol(seg_str, 0, 10);
    }

    CmdExtGeneralTestMode(pAd, mode, sub_mode);

    return TRUE;
}
#endif /* ERR_RECOVERY */

